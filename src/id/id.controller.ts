import { Controller, Get, Redirect } from '@nestjs/common';

@Controller('id')
export class IdController {
  @Get()
  @Redirect('https://connect.global.id/?client_id=c3a83b9a-538e-45e7-a371-b426431fdc56&response_type=code&scope=public&redirect_uri=http://localhost:3000&qr_only=true', 302)
  getId(): string {
    return 'Hello ID or some proof of it';
  }
}
